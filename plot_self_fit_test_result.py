import matplotlib.pyplot as plt
from glob import glob
import json
import numpy as np


ox, oy = 1, 1
path = f"out/self_fit_test/ox_{ox}_oy_{oy}"
mu_in, mu_out, betas = [], [], []
for f in glob(path + "/beta*"):
    data = json.load(open(f"{f}/fitres.json"))
    mu_in.append(data["input_info"]["mu"])
    mu_out.append(data["mu"])
    betas.append(data["input_info"]["beta"])
betas = np.array(betas)
mu_in = np.array(mu_in)
mu_out = np.array(mu_out)

fig, ax = plt.subplots()
fig.set_size_inches(11.7, 8.3*0.8)
ax.scatter(betas*180/np.pi, mu_in, label=r"$\mu$ input")
ax.scatter(betas*180/np.pi, mu_out, marker="x", label=r"$\mu$ fit")
ax.set_xlabel(r"$\beta$ in degree")
ax.set_ylabel("$\mu$")
fig.savefig(f"{path}/mus_summary.pdf")

fig, ax = plt.subplots()
fig.set_size_inches(11.7, 8.3*0.8)
plt.hist(mu_out - mu_in)
fig.savefig(f"{path}/mus_summary_hist.pdf")
plt.show()
