import numpy as np
import matplotlib.pyplot as plt
import json
from scipy.optimize import minimize
import matplotlib as mpl
from util import mkdirs
import config


def calc_mirror_distance(ox, oy):
    return config.d*(ox + oy)/np.sqrt(2)

def calc_mu_theory(md, alpha, beta, lmb):
    return md/lmb*(np.sin(alpha) + np.sin(beta))

def calc_diff_pattern(N, pad, theta, beta, ox, oy):
    alpha = 2*config.theta - beta
    md = calc_mirror_distance(ox, oy)
    e = np.cos(theta - beta)
    d = 1/(N - 1)
    x = np.linspace(d/2, 1 - d/2, N)
    r = x[None, :] < e*x[:, None]
    r = np.append(r, np.flip(r, axis=0), axis=0)
    r = np.append(np.flip(r, axis=1), r, axis=1)
    z = np.zeros((pad, pad), dtype=np.complex128)
    o = pad//2 - N
    z[o:o + 2*N, :2*N] = r
    x = round((ox + oy)*np.cos(beta)*N)
    y = o + round((ox - oy)*N)
    mu = calc_mu_theory(md, alpha, beta, config.lmb)
    z[y:y+2*N, x:x+2*N] += r*np.exp(2j*np.pi*mu)
    if False:
        plt.imshow(np.abs(z))
        plt.show()
    mu = mu - int(mu)
    print(f"mu (simulation): {mu:.4g}")
    I = np.abs(np.fft.fftshift(np.fft.fft2(z)))**2
    return I, mu

def fitfunc(x, y, w, k, gamma, theta, beta, mx, my, sx, sy, k_corr, mu):
    kx = k*np.cos(gamma)
    ky = k*np.sin(gamma)
    xf = sx*(x - mx)
    yf = sy*(y - my)
    e = np.cos(theta - beta)
    a = (xf[None, :]*e + yf[:, None])/np.sqrt(2)
    b = (xf[None, :]*e - yf[:, None])/np.sqrt(2)
    return (np.sinc(w*a)*np.sinc(w*b)*np.cos(k_corr*(kx*xf[None, :] + ky*yf[:, None]) - np.pi*mu))**2

def do_fit(img, ox, oy, theta, beta, skip_fit=False):
    x = np.arange(img.shape[1])
    y = np.arange(img.shape[0])
    img /= np.sum(img)
    x = x - np.sum(x[None, :]*img)
    y = y - np.sum(y[:, None]*img)
    md = calc_mirror_distance(ox, oy)
    c = config.pixel_size*config.k*config.M/config.f
    k = md*c*np.cos(beta)/2 # this is the k of the E field on the camera (in pixel coordinates), if there were only to small hole apertures
    w = config.d*c/(2*np.pi) # for the sinc envelope
    a = (ox + oy)/np.sqrt(2)
    b = (ox - oy)/np.sqrt(2)
    gamma = np.arctan(b/a)
    if False:
        z = fitfunc(x, y, w, k, gamma, theta, beta, 0, 0, 1, 1, 1, 0)
        z /= np.sum(z)
        fig, axs = plt.subplots(1, 2)
        m = max([np.max(z), np.max(img)])
        axs[0].matshow(z, vmin=0, vmax=m)
        axs[1].matshow(img, vmin=0, vmax=m)
        plt.show()
    N = 20
    mus = np.linspace(0, 1, N, endpoint=False)
    chi2, Ns = [], []
    for mu in mus:
        z = fitfunc(x, y, w, k, gamma, theta, beta, 0, 0, 1, 1, 1, mu)
        N = np.sum(z)
        z /= N
        Ns.append(N)
        chi2.append(np.sum((z - img)**2))
    if False:
        plt.plot(mus, chi2)
        plt.show()
    b = np.argmin(chi2)
    initial_mu = mus[b]
    N = Ns[b]
    p = (1, 0, 0, 1, 1, 1, initial_mu)
    if not skip_fit:
        fitres = minimize(lambda p: ((p[0]*fitfunc(x, y, w, k, gamma, theta, beta, *p[1:])/N - img)**2).sum(), p,
                          method="BFGS", options={"gtol": 1e-8})
        p = fitres.x
        f = fitres.fun
        mu = p[6]
        success = fitres.success
        message = fitres.message
    else:
        mu = initial_mu
        f = chi2[b]
        success = False
        message = "skipped fit"
    print(f"mu (fit) (ox={ox}, oy={oy}): {mu:.4g}")
    final_fitfunc = p[0]*fitfunc(x, y, w, k, gamma, theta, beta, *p[1:])/N
    final_params = {"N": p[0], "mx": p[1], "my": p[2], "sx": p[3], "sy": p[4],
                    "k": k*p[5], "mu": p[6], "initial_mu": initial_mu, "skip_fit": skip_fit,
                    "initial_chi2": chi2[b], "final_chi2": f, "success": success,
                    "message": message, "k_corr:": p[5]}
    return final_fitfunc, final_params

def export_fit_summary(path, img, final_fitfunc, final_params, input_info):
    mkdirs(path)
    fitres = dict(final_params)
    fitres["input_info"] = input_info
    json.dump(fitres, open(f"{path}/fitres.json", "w"), indent=4)
    m = max([np.max(final_fitfunc), np.max(img)])
    norm_io = mpl.colors.Normalize(vmin=0, vmax=m)
    fig, axs = plt.subplots(2, 2)
    fig.subplots_adjust(left=0.15, right=0.85, top=0.95, bottom=0.03, hspace=0.01, wspace=0.4)
    fig.set_size_inches(11.7, 8.3*0.8)
    axs[0, 0].matshow(final_fitfunc, norm=norm_io)
    axs[0, 0].set_title(r"fitted fitfunction, $\hat{I}(x, y)$", {'fontsize': 13}, pad=0)
    axs[0, 1].matshow(img, norm=norm_io)
    axs[0, 1].set_title("data, $I(x, y)$", {'fontsize': 13}, pad=0)
    diffimg = axs[1, 0].matshow((final_fitfunc - img)/(final_fitfunc + img))
    axs[1, 0].set_title(r"$(\hat{I}(x, y) - I(x, y))/(\hat{I}(x, y) + I(x, y))$", {'fontsize': 13}, pad=0)
    cax = fig.add_axes([axs[0, 1].get_position().x1 + 0.02, axs[0, 1].get_position().y0, 0.02, axs[0, 1].get_position().y1 - axs[0, 1].get_position().y0]) 
    fig.colorbar(mpl.cm.ScalarMappable(norm=norm_io), cax=cax)
    cax = fig.add_axes([axs[1, 0].get_position().x1 + 0.02, axs[1, 0].get_position().y0, 0.02, axs[1, 0].get_position().y1 - axs[1, 0].get_position().y0]) 
    fig.colorbar(diffimg, cax=cax)
    for t in axs:
        for ax in t:
            ax.set_axis_off()
    fig.savefig(f"{path}/summary.pdf")
    plt.close(fig)

