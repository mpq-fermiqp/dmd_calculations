import matplotlib.pyplot as plt
import numpy as np
from scipy.optimize import root_scalar
import config

theta = 12*np.pi/180

def calc_m(alpha):
    beta = 2*theta - alpha
    return config.d/config.lmb*(np.sin(alpha) + np.sin(beta))/np.sqrt(2)

def search_opt(bracket, order):
    a = root_scalar(lambda a: calc_m(a) - order, bracket=bracket).root
    b = 2*theta - a
    print(f"order: {order}, alpha: {a*180/np.pi:.4g}, beta: {b*180/np.pi:.4g}, diff: {(b - a)*180/np.pi:.4g}")
    return a, b

alpha = np.linspace(np.pi/2, -np.pi/2, 1000)
m = calc_m(alpha)
angles = []
for i in range(1, int(np.max(m)) + 1):
    angles.append(search_opt((-np.pi/2, 0), i))
    angles.append(search_opt((np.pi/2, 0), i))
a, b = angles[-1]
fig, ax = plt.subplots()
fig.set_size_inches(11.7, 8.3*0.8)
ax.plot(alpha*180/np.pi, m, linewidth = 2)
ax.set_ylabel(r'$\mu$')
ax.set_xlabel(r'$\alpha$ in degree')
ax.grid()
fig.tight_layout()
fig.savefig(f"out/dmd_diffraction_order.pdf")
plt.show()

for i in range(int(np.max(m))):
    alpha_i = np.arcsin(i*np.sqrt(2)*config.lmb/config.d - np.sin(b))
    print(f"order: {i} at {(alpha_i - a)*180/np.pi:.4g} relative to alpha")
exit_angle = min(*angles[-1])
mirrors_per_tweezer = 10
M = mirrors_per_tweezer*config.d/config.a
f2 = M*config.EFL
print(f"M: {M:.4g}, f2: {f2/1e3:.4g}mm")
tweezer_dist_on_dmd = config.a*np.sqrt(2)*M
focal_shift_on_dmd = -tweezer_dist_on_dmd*np.tan(exit_angle)
print(f"focal shift per tweezer (along bad axis): {focal_shift_on_dmd/M**2:.4g}um")
kk = config.k/config.EFL
Delta = 500e3
fov_radius = 20*config.a
phi = lambda x: Delta*config.k*np.sqrt(1 - x/config.EFL)
print(f"wavefront phase error across tweezer (center of fov): {phi(0) - phi(config.a/2):.4g}rad")
print(f"wavefront phase error across tweezer (edge of fov): {phi(fov_radius) - phi(config.a + fov_radius):.4g}rad")
