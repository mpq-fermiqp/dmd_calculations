import matplotlib.pyplot as plt
import numpy as np
from scipy.optimize import root_scalar
import config


theta = 12.25*np.pi/180
alpha = 37.1*np.pi/180
f2 = 150e3
N = 10
N_padding = 100
res = 5
conf = np.zeros((N, N))
s = 1
d = 2
conf[N//2:N//2+s, N//2:N//2+s] = 1
conf[N//2:N//2+s, N//2+d:N//2+d+s] = 1
lens_radius = 50.8/2*1e3

n = np.arange(N)
dist = (n[:, None] - n[None, :])*config.d/np.sqrt(2)
beta = 2*theta - alpha
phi = conf*np.exp(2j*np.pi*dist/config.lmb*(np.sin(alpha) + np.sin(beta)))
plt.matshow(np.real(phi))
plt.show()
phi = np.pad(phi, N_padding)
cell = np.ones((res, res))
E = phi[:, :, None, None]*cell
Ntot = N + 2*N_padding
E = E.swapaxes(1, 2).reshape(Ntot*res, Ntot*res)

fig, ax = plt.subplots()
I = np.abs(np.fft.fftshift(np.fft.fft2(E)))**2
dx = config.d/res
half_extent = f2*config.lmb/(2*dx)
ax.imshow(I, extent=(-half_extent/1e4, half_extent/1e4, -half_extent/1e4, half_extent/1e4))
ax.add_patch(plt.Circle((0, 0), lens_radius/1e4, lw=3, fill=False, edgecolor="red"))
ax.set_xlabel("x in cm")
ax.set_ylabel("y in cm")
plt.show()


