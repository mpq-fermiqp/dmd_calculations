import dmd_fitter as fit
import config
import numpy as np
import gc


ox, oy = 1, 1
out_path = f"out/self_fit_test/ox_{ox}_oy_{oy}"

oversample = 8
N = round(oversample*config.w*config.d*config.pixel_size*config.k*config.M/(2*np.sqrt(2)*np.pi*config.f))  # N is half the diagonal mirror size is grid points
betas = np.linspace(-5, 5, 10)*np.pi/180 - 13.1*np.pi/180
for beta in betas:
    I, mu = fit.calc_diff_pattern(N, oversample*config.w, config.theta, beta, ox, oy)
    w = (oversample*(config.w - config.h))//2
    c = (config.w*oversample)//2
    I = I[c - config.h//2:c - config.h//2 + config.h, c - config.w//2:c - config.w//2 + config.w]
    final_fitfunc, final_params = fit.do_fit(I, ox, oy, config.theta, beta)
    fit.export_fit_summary(f"{out_path}/beta_{beta:.4g}", I, final_fitfunc, final_params, {"mu": mu, "beta": beta})
    gc.collect()
