import math

d = 7.56 # dmd mirror pitch in microns
theta = 12/180*math.pi # NOMINAL dmd mirror tilt
pixel_size = 3.45
lmb = 0.671
k = 2*math.pi/lmb
w = 1440
h = 1080
M = 9.4
f = 150e3
a = 1.1
EFL = 22e3
