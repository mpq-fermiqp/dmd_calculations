import dmd_fitter as fit
import numpy as np
import json
import matplotlib.pyplot as plt
import config
import dmd_fitter as fit
import os
import config
import gc


run = "run_2023-09-05T21;46;24.675837_41"
in_path = f"data/{run}"
out_path = f"out/DMD_fit_result/{run}"
metadata = json.load(open(in_path + "/metadata.json"))
rng = metadata["range"]
alpha = metadata["incidence_angle_degree"]*np.pi/180
bg = np.load(in_path + "/background.npy")
software_int = metadata["software_int"]
offset_vals = [(1, 1)]
for x in range(-rng, rng + 1):
    for y in range(-rng, rng + 1):
        for ox, oy in offset_vals:
            outdir = f"{out_path}/x_{x}_y_{y}_ox_{ox}_oy_{oy}"
            if os.path.exists(outdir):
                print(f"skipping {outdir} as path exists")
                continue
            try:
                dimg = np.load(in_path + f"/x_{x}_y_{y}_ox_{oy}_oy_{ox}.npy")  # the experimental and internal ox, oy are confused: need to swap
            except FileNotFoundError:
                assert abs(ox - oy) <= 1
                d = ox + oy
                dimg = np.load(in_path + f"/x_{x}_y_{y}_d_{d}.npy")
            img = dimg - bg
            img = np.ma.array(img, mask=(img < 0) | (dimg == 1022*software_int) | (bg == 1022*software_int))
            if False:
                plt.matshow(img)
                plt.show()
            beta = 2*config.theta - alpha
            final_fitfunc, final_params = fit.do_fit(img, ox, oy, config.theta, beta)
            fit.export_fit_summary(outdir, img, final_fitfunc, final_params, metadata)
            gc.collect()

