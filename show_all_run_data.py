import dmd_fitter as fit
import numpy as np
import json
import matplotlib.pyplot as plt
import config
import dmd_fitter as fit
import os
import config
import gc


run = "run_2023-09-05T21;46;24.675837_41"
in_path = f"data/{run}"
metadata = json.load(open(in_path + "/metadata.json"))
rng = metadata["range"]
bg = np.load(in_path + "/background.npy")
software_int = metadata["software_int"]
ox, oy = (1, 1)
for x in range(-rng, rng + 1):
    for y in range(-rng, rng + 1):
        dimg = np.load(in_path + f"/x_{x}_y_{y}_ox_{oy}_oy_{ox}.npy")  # the experimental and internal ox, oy are confused: need to swap
        img = dimg - bg
        img = np.ma.array(img, mask=(img < 0) | (dimg == 1022*software_int) | (bg == 1022*software_int))
        plt.matshow(img)
        plt.show()
