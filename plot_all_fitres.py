import matplotlib.pyplot as plt
import numpy as np
import json
import dmd_fitter as fit
import config
from scipy.optimize import minimize


ox, oy = 1, 1
r = 3
mu_offset = 5
runs = ["run_2023-09-04T16;51;29.736901_33",
        "run_2023-09-05T18;36;44.980746_35",
        "run_2023-09-05T20;44;55.150154_37",
        "run_2023-09-05T21;23;59.054040_39",
        "run_2023-09-05T21;46;24.675837_41"]


def collect(run):
    data = []
    alpha = None
    for x in range(-r, r + 1):
        for y in range(-r, r + 1):
            path = f"out/DMD_fit_result/{run}/x_{x}_y_{y}_ox_{ox}_oy_{oy}/fitres.json"
            try:
                fitres = json.load(open(path))
            except FileNotFoundError:
                print(f"not found: {path}")
                continue
            if (fitres["sx"] < 0.5 or fitres["sx"] > 1.5 or
                fitres["sy"] < 0.5 or fitres["sy"] > 1.5 or
                fitres["N"] < 0.3 or fitres["N"] > 2.8 or
                fitres["k_corr:"] < 0.8 or fitres["k"] > 1.2):
                print(f"filtered: {path}, {fitres}")
                continue
            alpha2 = fitres["input_info"]["incidence_angle_degree"]*np.pi/180
            if alpha is None:
                alpha = alpha2
            else:
                assert alpha == alpha2
            mu = fitres["mu"]
            mu -= int(mu)
            if mu < 0:
                mu = 1 + mu
            if mu < 0.4:
                mu += 1
            data.append({"mu": mu, "k_corr": fitres["k_corr:"]})
    return {"data": data, "alpha": alpha}


rundata = []
for run in runs:
    rundata.append(collect(run))
props = ["k_corr", "mu"]
for prop in props:
    fig, ax = plt.subplots()
    for data, run in zip(rundata, runs):
        vals = np.array([d[prop] for d in data["data"]])
        ax.hist(vals, label=f"$\\alpha={data['alpha']*180/np.pi:.0f}^\\circ$", histtype="step", lw=3)
        x = np.mean(vals)
        std = np.std(vals)
        err = std/np.sqrt(len(vals))
        print(f"{run}: {prop}={x:.3g}+-{err:.1g} ({err/x*100:.2g}%, std={std:.4g})")
    ax.legend()
    ax.grid()
    ax.set_title(prop)

alphas, mean, sigma = [], [], []
for data in rundata:
    alphas.append(data["alpha"])
    mus = 2 - np.array([d["mu"] for d in data["data"]])
    mean.append(np.mean(mus))
    sigma.append(np.std(mus)/np.sqrt(len(mus)))
md = fit.calc_mirror_distance(ox, oy)
alphas = np.array(alphas)
mean = np.array(mean)
sigma = np.array(sigma)
mean += mu_offset

def fitfunc(theta):
    mus_theo = fit.calc_mu_theory(md, alphas, 2*theta - alphas, config.lmb)
    return np.sum(((mus_theo - mean)/sigma)**2)

fitres = minimize(fitfunc, [config.theta])
print(fitres)
theta_fit = fitres.x[0]
print(f"theta: {theta_fit*180/np.pi:.2f}°")
fig, ax = plt.subplots()
x = np.linspace(32*np.pi/180, 42*np.pi/180)
y = fit.calc_mu_theory(md, x, 2*theta_fit - x, config.lmb)
ax.plot(x*180/np.pi, y, label=f"fit ($\\theta={theta_fit*180/np.pi:.1f}^\\circ)$")
y = fit.calc_mu_theory(md, x, 2*config.theta - x, config.lmb)
ax.plot(x*180/np.pi, y, label=f"nominal ($\\theta={config.theta*180/np.pi:.1f}^\\circ)$")
ax.errorbar(alphas*180/np.pi, mean, sigma, capsize=3, ls="None", marker="o", label="data")
ax.set_xlim(x[0]*180/np.pi, x[-1]*180/np.pi)
ax.set_xlabel(r"$\alpha$")
ax.set_ylabel(r"$\mu$")
ax.legend()
ax.grid()
plt.show()
